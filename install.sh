#!/usr/bin/env bash

set -euo pipefail

[[ -z "${USERNAME+z}" ]] && >&2 echo "USERNAME is required" && exit 1
[[ -z "${PASSWORD+z}" ]] && >&2 echo "PASSWORD is required" && exit 1

[[ -z "${DAPPL_OS_TYPE+z}" ]] && >&2 echo "DAPPL_OS_TYPE is required" && exit 1
[[ "${DAPPL_OS_TYPE}" != "linux" ]] && >&2 echo "OS type is not linux" && exit 1

[[ -z "${DAPPL_OS_ARCH+z}" ]] && >&2 echo "DAPPL_OS_ARCH is required" && exit 1
[[ "${DAPPL_OS_ARCH}" != "amd64" ]] && >&2 echo "architecture is not amd64" && exit 1

[[ -z "${DAPPL_OS_DISTRO+z}" ]] && >&2 echo "DAPPL_OS_DISTRO is required" && exit 1
[[ "${DAPPL_OS_DISTRO}" != "ubuntu" ]] && >&2 echo "distribution must be ubuntu" && exit 1

[[ $EUID -ne 0 ]] && >&2 echo "must be run as root user" && exit 1

cd "$(dirname "${0}")"

# Prerequisites
echo "-------------------------------------------------------------------------------"
echo "Prerequisites"
echo "-------------------------------------------------------------------------------"
apt-get -qq update
apt-get -qq install apt-transport-https gnupg2 vim
curl -fsSL https://github.com/rabbitmq/signing-keys/releases/download/2.0/rabbitmq-release-signing-key.asc | sudo apt-key add -

# Data Disk Setup
echo "-------------------------------------------------------------------------------"
echo "Disk Setup"
echo "-------------------------------------------------------------------------------"
echo 'type=83' | sfdisk "$(./tmdisk 1)"
mkfs.ext4 "$(./tmdisk 1)1"
echo "UUID=$(blkid -s UUID -o value "$(./tmdisk 1)1") /var/lib/rabbitmq ext4 defaults 0 2" | tee -a /etc/fstab
mkdir -p /var/lib/rabbitmq
mount /var/lib/rabbitmq

# Erlang && RabbitMQ Install
echo "-------------------------------------------------------------------------------"
echo "RabbitMQ Install"
echo "-------------------------------------------------------------------------------"
echo "deb https://dl.bintray.com/rabbitmq-erlang/debian focal erlang-23.x" > /etc/apt/sources.list.d/bintray.erlang.list
echo "deb https://dl.bintray.com/rabbitmq/debian bionic main" > /etc/apt/sources.list.d/bintray.rabbitmq.list
apt-get -qq update
apt-get -qq install rabbitmq-server

# Setup RabbitMQ management
echo "-------------------------------------------------------------------------------"
echo "RabbitMQ Management"
echo "-------------------------------------------------------------------------------"
rabbitmq-plugins enable rabbitmq_management
rabbitmqctl add_user "${USERNAME}" "${PASSWORD}"
rabbitmqctl set_user_tags "${USERNAME}" administrator
rabbitmqctl set_permissions -p / "${USERNAME}" ".*" ".*" ".*"
